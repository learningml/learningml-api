# Contributing to LearningML-API

We love your input! We want to make contributing to this project as easy and transparent as possible. The guidelines to contribute to this project are the
same as in any other LearningML project. So we link to the [CONTRIBUTING.md](https://gitlab.com/learningml/learningml-editor/-/blob/master/CONTRIBUTING.md) file of [LearningML-editor](https://gitlab.com/learningml/learningml-editor) project.

You have to take into account the following differences with respect to that 
CONTRIBUTING.md file:

- The list of issues for this project is accesed through this link:
  https://gitlab.com/learningml/learningml-api/-/issues


