# Generated by Django 2.2.7 on 2020-01-22 07:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0007_project_reinventions'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='type',
            field=models.CharField(default='text', max_length=40),
        ),
    ]
