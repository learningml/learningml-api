# Generated by Django 2.2.7 on 2019-11-27 15:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0006_project_likes'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='reinventions',
            field=models.IntegerField(default=0),
        ),
    ]
