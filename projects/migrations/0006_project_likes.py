# Generated by Django 2.2.7 on 2019-11-26 06:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0005_project_shared'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='likes',
            field=models.IntegerField(default=0),
        ),
    ]
