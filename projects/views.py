import uuid
import threading

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.http import Http404
from django.shortcuts import render
from django.views.generic import TemplateView
from django.conf import settings

from .models import Project, Profile
from .serializers import ProjectSerializer, ProjectMetaDataSerializer, UserSerializer
from .sendmail import send_signup_email, send_bug_email


class SignupView(TemplateView):

    def get(self, request, activation_token):

        try:
            profile = Profile.objects.get(activation_token=activation_token)
            profile.active = True
            profile.save()
            context = {'username': profile.user.username, 'url_lml': settings.URL_LEARNINGML}
            return render(request, 'activated_account.html', context)
        except Profile.DoesNotExist:
            return render(request, 'activated_account_error.html')


class ApiSendBugView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        user = request.user
        title = request.data['title']
        description = request.data['description']

        thread = threading.Thread(target=send_bug_email,
                                  args=(user, title, description))
        thread.daemon = True
        thread.start()

        return Response({"sent": "ok"}, status=status.HTTP_201_CREATED)


class ApiSignupView(APIView):

    def post(self, request):
        userName = request.data['username']
        userPass = request.data['password']
        userMail = request.data['email']
        userMonth = request.data['month']
        userYear = request.data['year']
        userGender = request.data['gender']

        try:
            user = User.objects.create_user(userName, email=userMail, password=userPass)
            user.profile.month = userMonth
            user.profile.year = userYear
            user.profile.gender = userGender
            user.save()
            serializer = UserSerializer(user)

            # enviamos el email en otro hilo para no bloquear el hilo principal
            thread = threading.Thread(target=send_signup_email,
                                      args=(user,))
            thread.daemon = True
            thread.start()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except IntegrityError as e:
            return Response("Ese nombre de usuario ya existe", status=status.HTTP_409_CONFLICT)
        except Exception as e:
            return Response(repr(e), status=status.HTTP_400_BAD_REQUEST)


class ApiUserView(APIView):

    def get(self, request, username):
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            return Response(0, status=status.HTTP_200_OK)


        return Response(user.id, status=status.HTTP_200_OK)


class ApiProjectListView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):

        try:
            shared = request.GET['shared']
        except Exception:
            shared = False

        print(shared)

        if(shared):
            projects = Project.objects.filter(shared=1)
            serializer = ProjectSerializer(projects, many=True)
        else:
            projects = Project.objects.filter(user=request.user)
            serializer = ProjectMetaDataSerializer(projects, many=True)

        return Response(serializer.data)


    def post(self, request):
        data = request.data
        data['user'] = request.user.id
        data['uuid'] = str(uuid.uuid1())

        print(request.user.profile.active)
        serializer = ProjectSerializer(data=data)

        if serializer.is_valid():
            if not request.user.profile.active:
                return Response(serializer.errors, status=status.HTTP_401_UNAUTHORIZED)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ApiProjectDetailView(APIView):
    permission_classes = (IsAuthenticated,)

    def get_object(self, pk):
        try:
            return Project.objects.get(pk=pk)
        except Project.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        project = self.get_object(pk)
        serializer = ProjectSerializer(project)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        user = request.user
        project = self.get_object(pk)

        serializer = ProjectSerializer(project, data=request.data)

        if user != project.user:
            return Response(["No puedes modificar un proyecto que no es tuyo"],
                            status=status.HTTP_403_FORBIDDEN)

        if serializer.is_valid():
            if not request.user.profile.active:
                return Response(serializer.errors, status=status.HTTP_401_UNAUTHORIZED)
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        user = request.user

        project = self.get_object(pk)
        if user != project.user:
            return Response(["No puedes eliminar un proyecto que no es tuyo"],
                            status=status.HTTP_403_FORBIDDEN)
        project.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ApiProjectCommunityOperations(APIView):
    permission_classes = (IsAuthenticated,)

    def get_object(self, pk):
        try:
            return Project.objects.get(pk=pk)
        except Project.DoesNotExist:
            raise Http404

    def put(self, request, pk, operation):
        project = self.get_object(pk)
        if operation == 'reinvent':
            project.reinventions = project.reinventions + 1
        elif operation == 'like':
            project.likes = project.likes + 1
        else:
            return Response({'error': 'operación no permitida' }, status=status.HTTP_400_BAD_REQUEST)

        project.save()
        return Response({'reinventions': project.reinventions }, status=status.HTTP_200_OK)