from django.views.generic import TemplateView
from django.shortcuts import render
from django.conf import settings


class HomeView(TemplateView):
    #template_name = 'home.html'
    template_name = 'redirect_to_wordpress.html'

    # def get(self, request):
    #
    #     context = {
    #         'url_lml_signup': "{}/signup".format(settings.URL_LEARNINGML),
    #         'url_lml_editor': settings.URL_LEARNINGML
    #     }
    #     return render(request, 'home.html', context)

class AvisoLegalView(TemplateView):

    def get(self, request):
        return render(request, 'aviso-legal.html')

class AprendeView(TemplateView):

    def get(self, request):
        return render(request, 'aprende.html')

class SobreView(TemplateView):

    def get(self, request):
        return render(request, 'sobre.html')