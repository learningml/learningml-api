# LearningML API

## Introduction

LearningML is an educative platform intended to teach and learn Machine Learning
(ML) fundamentals through hand-on activities. The full platform is composed of
four parts:

- a web site (https://learnigml.org)
- a Machine Learning editor (this repo contains the code)
- a programming editor, which is a modification of the Scratch project
      (https://scratch.mit.edu/) (https://gitlab.com/learningml/learningml-scratch-gui, https://gitlab.com/learningml/learningml-scratch-vm, https://gitlab.com/learningml/learningml-scratch-l10)
- an API server application (this repo).

The  ML editor allows to build ML models from labeled datasets of texts or images.
The programming editor allows to code applications able to use these ML models.
And the server application is intended to register users and allows them to 
save and share their projects. 

The main design principle followed when developing LearningML has been simplicity.
The main goal has been to build a tool very easy to start with as well as getting
some results quickly and easily. Hence, it is a platform specially appropriate to 
learn and teach ML fundamentals to children.

LearningML is also a computing education research project aiming to discover
effective ways to learn and teach artificial intelligence and Machine Learning
fundamentals at school.

And, finally, LearningML is an open source project wishing to grow with the help
of everyone interested in contributing. 

You can install the ML editor, the programming editor and the API server in
your computer, study the code and modify it in the terms gathered in their licenses (Affero GNU for the ML editor and BSD-3 for the programming editor).

There is also a cloud instance of the platform freely (as in gratis) available
to everybody who wants to use it. You can get access by pointing your browser to
https://learnigml.org/editor.

You can find more information about LearningML project in the web site 
(https://learnigml.org).


## Getting started

Clone the repo.

    # git clone https://gitlab.com/learningml/learningml-api.git
    # cd learningml-api

Although not required, it is strongly recommended to use a python virtual 
enviroment to install and run the application. If you don't know how to do it, 
here you have some useful links to learn:

- https://docs.python.org/es/3/library/venv.html
- https://realpython.com/python-virtual-environments-a-primer/


Install dependencies:

    # pip install requirements.txt
  
Create a MySQL database and modify `lml_api/settings.py` file to allow Django
project to access it. 

    ...
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'learningml',
            'USER': 'root',
            'PASSWORD': 'root',
            'HOST': '127.0.0.1',
            'PORT': '8889',
            'OPTIONS': {
                # Tell MySQLdb to connect with 'utf8mb4' character set
                'charset': 'utf8mb4',
            },
        },
    }
    ...

Run migrations to populate database.

    # python manage.py migrate

Run the Django's development server.

    # python manage.py runserver
    
Create a superuser to enter admin application.

    # python manage.py createsuperuser

Now you can enter the admin app pointing a web browser to the url

    - http://localhost:8000/admin
    

## API routes

### Getting the token

`POST /api-token-auth`

body

`{"username": "ciro", "password": "1dcnnqa2"}`

### Registering a user

`POST /projects/signup`

body

`{"username": "ciro", "password": "1dcnnqa2", "email": "ciro@kk.es", "month": "5", "year": "2014", "gender": "M"}`

### Check if a user exists

`GET /projects/username/[username]`

### Get a project

`GET /projects/{id}`

headers

`Authoritation: Token 1c840aa735f78678491c6a77583d8539bb00fecd`

### Get all user projects

`GET /projects`

headers

`Authoritation: Token 1c840aa735f78678491c6a77583d8539bb00fecd`


### Get all shared projects

`GET /projects?shared=1`

headers

`Authoritation: Token 1c840aa735f78678491c6a77583d8539bb00fecd`

### Send a new project

`POST /projects`

headers

`Authoritation: Token 1c840aa735f78678491c6a77583d8539bb00fecd`

body

`{"name":"el proyectongazo ","description":"isisisisisissdsada", "shared":"0", "json_data":"{\"lilu\":\"lala\", \"klkl\":\"jdd\"}"}`

### Modify a project

`PUT /projects/{id}`

headers

`Authoritation: Token 1c840aa735f78678491c6a77583d8539bb00fecd`

body

`{"name":"el proyectongazo modificado","description":"isisisisisissdsada","json_data":"{\"lilu\":\"lala\", \"klkl\":\"jdd\"}"}`

## Delete a project

`DELETE /projects/{id}`

headers

`Authoritation: Token 1c840aa735f78678491c6a77583d8539bb00fecd`

## Add a reinvention fo a project

`PUT /projects/operation/{id}/{operation}`

`{operation}` can should be

 - reinvent, suma 1 a reinventions
 - like, suma 1 a likes

headers

`Authoritation: Token 1c840aa735f78678491c6a77583d8539bb00fecd`


# Affero GPL License

LearningML, the easiest way to learn Machine Learning fundamentals

Copyright (C) 2020 Juan David Rodríguez García & KGB-L3

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.